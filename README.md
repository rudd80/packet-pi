# PacketPi

PacketPi is a configuration script to setup a Raspberry Pi as an interface to a HAM radio packet TNC. It installs all the necessary software (D-RATS) and configuration files to get you up and running. Includes a wifi configuration so that you can transfer files between your smartphone and Pi. The intent was to provide a small and simple way to deploy a packet radio setup during an emgergency. The smartphone integration can be usefull to send photos and documents over the air to the EOC (Emergency Operations Center).

## Prerequisites

### Raspberry Pi

The script requires a Raspberry Pi with the latest Raspbian PIXEL distribution. The Raspberry Pi 3 B is recommended. You can run it on an older Raspberry Pi but you will need a wifi card to use the wifi options. Otherwise, you can just skip the wifi and just use the D-RATS configuration.

### TNC

You need a TNC that supports KISS mode. Most KISS based TNCs should work. The the [TNC-X](https://www.tnc-x.com/) and the [Kantronics KPC-3+](http://www.kantronics.com/products/kpc3.html) have been verified to work. While there is a version of the TNC-X that fits on top of the Raspberry Pi, I recommend the USB with Enclosure version since you can use that to connect to a PC as well, if needed.

If you need a serial to USB adaptor cable for your TNC (not needed with the TNC-X), make sure to get a high quality one with a [FTDI chipset](http://a.co/8W6dLH4) or [this one](http://a.co/1UEpD5U).

The serial connection from the computer to the TNC should be set to 9600 baud. Some auto-negotiate (KPC3+) but some you have to reconfigure a jumper on the board (TNC-X).

If you are not using a KISS-only TNC, such as the TNC-X, make sure KISS mode is enabled. If you are using the KPC3+, do not use Putty to enable KISS mode (it will hang the TNC and require a hard reset).

Follow the manual for your TNC (if available) to set the receive volume. The TNC-X requires the squelch all the way open while the KPC-3+ has a procedure to follow to set the correct volume.

## Optional Components

### LCD Monitor

If you want to go mobile with your setup, then you will need a tiny monitor. This is a good option and will be configured by the script: (http://a.co/1lLeUs8)

Another option is to enable VNC on the Pi and as long as you have the Wifi option enabled, you can connect via an app on your smartphone or tablet.

### Voltage Regulator

With my mobile setup I run everything off of 12v DC power. I found that the Pi likes a voltage closer to 5.25V so I got this voltage regulator as well: (http://a.co/c3st6Zu)

Use your multimeter to adjust the voltage to 5.25V and then build a micro USB cable that will connect the regulator to your Pi.

## Deployment

1. Download the latest version of Raspbian with PIXEL:
  * https://www.raspberrypi.org/downloads/raspbian/
1. Follow the instructions to write the image to your SD card:
  * https://www.raspberrypi.org/documentation/installation/installing-images/README.md
1. Connect the following to your Pi:
  * SD card that you just loaded with Raspbian
  * Keyboard
  * Mouse
  * Ethernet cable to an internet connection
  * Monitor
  * Connect the power cable and wait for it to boot-up
1. Open a terminal and run the following to install the latest patches:

    ```
    sudo apt-get update
    sudo apt-get -y upgrade
    ```

1. Change the password to the "pi" account:

    ```
    passwd pi
    ```
    
2. Reboot the Pi:

    ```
    sudo reboot
    ```

1. Open a terminal and run the following to download the configuration script:

    ```
    git clone https://gitlab.com/rudd80/packet-pi.git
    ```
1. Run the configuration script and answer the prompts:

    ```
    cd packet-pi
    sudo ./configurePacketPi
    ```

1. Once the script is done, plug-in your TNC and reboot the pi:

    ```
    sudo reboot
    ```

The Pi should boot-up and launch the D-RATS software. All done!

## Contributing

Merge requests are welcome but may not be approved as this project is tailored for a specific application. Feel free to fork it and and modify as needed.

## Versioning

No versioning at this time. Just pull the latest from the master branch.

## Authors

* [Brian Rudd](https://gitlab.com/rudd80)

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details

## Acknowledgments

* The Wifi setup was inspired by this [blog post](http://www.movingelectrons.net/blog/2016/06/26/backup-photos-while-traveling-with-a-raspberry-pi.html)
